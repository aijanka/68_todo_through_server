import {FETCH_REQUEST, FETCH_SUCCESS, SAVE} from "./actions";

const initialState = {
    tasks: [],
    currentTask: '',
    loading: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case SAVE:
            return {...state, currentTask: action.currentTask};
        case FETCH_REQUEST:
            return {...state, loading: true};
        case FETCH_SUCCESS:
            return {...state, tasks: action.tasks};
        default:
            return state;

    }
};

export default reducer;
