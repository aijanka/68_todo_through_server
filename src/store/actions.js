import axios from 'axios';

export const SAVE = 'SAVE_TASK_MESSAGE';
export const FETCH_REQUEST = 'FETCH_REQUEST';
export const FETCH_SUCCESS = 'FETCH_SUCCESS';
export const FETCH_ERROR = 'FETCH_ERROR';



export const saveTaskMessage = (event) => ({type: SAVE, currentTask: event.target.value});
export const fetchRequest = () => ({type: FETCH_REQUEST});
export const fetchSuccess = (tasks) => ({type: FETCH_SUCCESS, tasks: tasks || []});
export const fetchError = (error) => ({type: FETCH_ERROR, error});

export const fetchTasks = () => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.get('/tasks.json').then(
            response => {
                dispatch(fetchSuccess(response.data));
            }, error => {
                dispatch(fetchError(error));
            }
        )
    }
};

export const fetchAdd = () => {
    return(dispatch, getState) => {
        dispatch(fetchRequest());
        const task = {task: getState().currentTask};
        axios.post('/tasks.json', task).then(
            response => {
                dispatch(fetchTasks());
            }, error => {
                dispatch(fetchError(error));
            }
        )
    }

};

// export const saveTaskMessage = (event) => ({type: SAVE, currentTask: event.target.value})