import React from 'react';
import './InputPart.css';

const InputPart = props => (
    <div className="input-group mb-3">
        <input
            type="text"
            className="form-control"
            placeholder="Add new task"
            aria-label=""
            aria-describedby="basic-addon1"
            id='newTaskAdder'
            onChange={props.currentTask}
            value={props.value}
        />
        <div className="input-group-append">
            <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={props.addTask}
            >
                Add
            </button>
        </div>
    </div>
)

export default InputPart;