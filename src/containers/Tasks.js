import React, {Component} from 'react';
import InputPart from "../InputPart/InputPart";
import Task from "../Task/Task";
import '../App.css';

import {connect} from 'react-redux';
import {fetchAdd, fetchTasks, saveTaskMessage} from "../store/actions";

class Tasks extends Component {

    componentDidMount() {
        this.props.fetchTasks();
    }

    // deleteTask = (id) => {
    //     const index = this.state.tasks.findIndex(p => p.id === id);
    //     const tasks = [...this.state.tasks];
    //     tasks.splice(index, 1);
    //     this.setState({tasks});
    // };


    render() {
        return (
            <div className="App">
                <div className="container">
                    <InputPart
                        addTask={() => this.props.addTask()}
                        currentTask={(event) => this.props.saveTaskMessage(event)}
                        value={this.props.currentTask}/>
                    {Object.keys(this.props.tasks).map((id) =>
                        <Task
                            taskMessage={this.props.tasks[id].task}
                            key={id}
                            // deleteTask={() => this.deleteTask(task.id)}
                        />
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => (
    {
        tasks: state.tasks,
        currentTask: state.currentTask
    }
);

const mapDispatchToProps = dispatch => (
    {
        fetchTasks: () => dispatch(fetchTasks()),
        addTask: () => dispatch(fetchAdd()),
        saveTaskMessage: (event) => dispatch(saveTaskMessage(event))
    }
)

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);